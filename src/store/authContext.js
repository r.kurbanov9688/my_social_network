import React, { useContext, useState } from 'react';
import { TOKEN } from '../constants';

const AuthContext = React.createContext({
  token: '',
  isLoggedIn: false,
  logout: () => {},
  login: () => {},
});

export function AuthContextProvider(props) {
  const initialToken = localStorage.getItem(TOKEN);
  const [token, setToken] = useState(initialToken);

  const isLoggedIn = !!token;
  const loginHandler = (token) => {
    setToken(token);
    localStorage.setItem(TOKEN, token);
  };
  const logoutHandler = () => {
    setToken(null);
    localStorage.removeItem(TOKEN);
  };

  const contextValue = {
    token,
    isLoggedIn,
    logout: logoutHandler,
    login: loginHandler,
  };

  return (
    <AuthContext.Provider value={contextValue}>
      {props.children}
    </AuthContext.Provider>
  );
}

export const useAuthContext = () => {
  return useContext(AuthContext);
};
