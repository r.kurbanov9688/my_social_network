export const SIGN_UP_URL = `https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=${process.env.REACT_APP_API_KEY}`;
export const SIGN_IN_URL = `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${process.env.REACT_APP_API_KEY}`;
export const PROFILE_ROUTE = '/profile';
export const FRIENDS_ROUTE = '/friends';
export const MESSAGE_ROUTE = '/messages';
export const AUTH_ROUTE = '/auth';
export const TOKEN = 'token';
export const INVALID_EMAIL_ADDRESS = 'Invalid email address';
export const NO_EMAIL_PROVIDED = 'No email provided';
export const NO_PASSWORD_PROVIDED = 'No password provided.';
export const PASSWORD_IS_SHORT =
  'Password is too short - should be 8 chars minimum.';
export const CREATE_NEW_ACCOUNT = 'Create new account';
export const LOGIN = 'Login with existing account';
