import axios from 'axios';
import { useMutation } from 'react-query';
import { useAuthContext } from '../store/authContext';
import { SIGN_IN_URL, SIGN_UP_URL } from '../constants';

const signUpUser = (user) => axios.post(`${SIGN_UP_URL}`, user);

const signInUser = (user) => {
  return axios.post(SIGN_IN_URL, user);
};

export const useSignUpUser = () => useMutation(signUpUser);

export const useSignInUser = () => {
  const { login } = useAuthContext();
  return useMutation(signInUser, {
    onSuccess: (data) => {
      login(data.data.idToken);
    },
  });
};
