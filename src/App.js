import React from 'react';
import './App.scss';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { QueryClientProvider, QueryClient } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';
import { AuthContextProvider } from './store/authContext';
import { PrivateRoute } from './components/PrivateRoute/PrivateRoute';
import {
  AUTH_ROUTE,
  FRIENDS_ROUTE,
  MESSAGE_ROUTE,
  PROFILE_ROUTE,
} from './constants';
import { Profile } from './components/Profile/Profile';
import { Friends } from './components/Friends/Friends';
import { Messages } from './components/Messages/Messages';
import { Header } from './components/Header/Header';
import { Navigation } from './components/Navigation/Navigation';
import { Home } from './components/Home/Home';
import { AuthForm } from './components/AuthForm/AuthForm';

const queryClient = new QueryClient();

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <AuthContextProvider>
        <BrowserRouter>
          <div className="App">
            <Header />
            <Navigation />
            <div className="app-container">
              <Routes>
                <Route path="/" element={<Home />} />
                <Route
                  path={PROFILE_ROUTE}
                  element={<PrivateRoute Component={Profile} />}
                />
                <Route
                  path={FRIENDS_ROUTE}
                  element={<PrivateRoute Component={Friends} />}
                />
                <Route
                  path={MESSAGE_ROUTE}
                  element={<PrivateRoute Component={Messages} />}
                />
                <Route path={AUTH_ROUTE} element={<AuthForm />} />
              </Routes>
            </div>
          </div>
        </BrowserRouter>
      </AuthContextProvider>

      <ReactQueryDevtools initialIsOpen={false} />
    </QueryClientProvider>
  );
}

export default App;
