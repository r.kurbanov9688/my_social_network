import './Navigation.scss';
import { Link } from 'react-router-dom';
import { FRIENDS_ROUTE, MESSAGE_ROUTE, PROFILE_ROUTE } from '../../constants';

export const Navigation = () => {
  return (
    <div className="nav">
      <div>
        <Link to="/">Home</Link>
      </div>
      <div>
        <Link to={PROFILE_ROUTE}>Profile</Link>
      </div>
      <div>
        <Link to={MESSAGE_ROUTE}>Messages</Link>
      </div>
      <div>
        <Link to={FRIENDS_ROUTE}>Friends</Link>
      </div>
    </div>
  );
};
