import React from 'react';
import { Navigate } from 'react-router-dom';
import { useAuthContext } from '../../store/authContext';

export const PrivateRoute = ({ Component }) => {
  const { isLoggedIn } = useAuthContext();
  return isLoggedIn ? <Component /> : <Navigate to="/auth" />;
};
