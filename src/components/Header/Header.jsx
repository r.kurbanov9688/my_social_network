import './Header.scss';
import { Link } from 'react-router-dom';
import { useAuthContext } from '../../store/authContext';
import { AUTH_ROUTE } from '../../constants';

export const Header = () => {
  const { isLoggedIn, logout } = useAuthContext();
  return (
    <header className="header">
      <div className="login">
        {!isLoggedIn && <Link to={AUTH_ROUTE}>Log In</Link>}
        {isLoggedIn && <button onClick={logout}>Logout</button>}
      </div>
    </header>
  );
};
