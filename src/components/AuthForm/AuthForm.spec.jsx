import { AuthForm } from './AuthForm';
import user from '@testing-library/user-event';
import { QueryClient, QueryClientProvider, useMutation } from 'react-query';
import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { AuthContextProvider, useAuthContext } from '../../store/authContext';
import { NO_EMAIL_PROVIDED } from '../../constants';
import { waitFor } from '@testing-library/dom';
import { screen, render } from '@testing-library/react';
import { useSignInUser, useSignUpUser } from '../../hooks/useData';
import { useNavigate } from 'react-router';

jest.mock('../../hooks/useData', () => ({
  useSignInUser: jest.fn(),
  useSignUpUser: jest.fn(),
}));
jest.mock('react-router', () => ({
  useNavigate: jest.fn(),
}));
beforeEach(() => {
  useSignInUser.mockImplementation(() => {
    const { login } = useAuthContext();
    return {
      mutate: () => {
        login('testToken');
      },
    };
  });
  useSignUpUser.mockReturnValue({ mutate: jest.fn() });
  useNavigate.mockReturnValue(jest.fn());
});
describe('AuthForm', () => {
  const customRender = () => {
    render(
      <AuthContextProvider>
        <AuthForm />
      </AuthContextProvider>
    );
  };
  it('onSubmit is called when all fields pass validation', async () => {
    const navigate = jest.fn();
    useNavigate.mockReturnValue(navigate);

    customRender();
    await user.type(getEmail(), 'email@gmail.com');
    await user.type(getPassword(), 'qwe12EWQ');

    clickSubmitButton();

    // await waitFor(() => {
    //   expect(mutate).toHaveBeenCalledWith({
    //     email: 'email@gmail.com',
    //     password: 'qwe12EWQ',
    //     returnSecureToken: true,
    //   });
    //   expect(mutate).toHaveBeenCalledTimes(1);
    // });
    await waitFor(() => {
      expect(navigate).toHaveBeenCalledTimes(1);
    });
    await waitFor(() => {});
  });

  it.skip('should show error message below input', function () {
    customRender();
    user.type(getEmail(), '');
    user.type(getPassword());
    screen.getByText(NO_EMAIL_PROVIDED).toBeInTheDocument();
  });
});

function getEmail() {
  return screen.getByRole('textbox', {
    name: /your email/i,
  });
}

function getPassword() {
  return screen.getByLabelText(/your password/i);
}

function clickSubmitButton() {
  user.click(
    screen.getByRole('button', {
      name: /login/i,
    })
  );
}
