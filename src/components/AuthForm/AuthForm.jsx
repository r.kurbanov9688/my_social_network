import { useAuthContext } from '../../store/authContext';
import { useEffect, useState } from 'react';
import { useSignInUser, useSignUpUser } from '../../hooks/useData';
import './AuthForm.scss';
import {
  CREATE_NEW_ACCOUNT,
  INVALID_EMAIL_ADDRESS,
  LOGIN,
  NO_EMAIL_PROVIDED,
  NO_PASSWORD_PROVIDED,
  PASSWORD_IS_SHORT,
  PROFILE_ROUTE,
} from '../../constants';
import { useNavigate } from 'react-router';
import { useFormik } from 'formik';
import * as Yup from 'yup';

export const AuthForm = () => {
  const { isLoggedIn } = useAuthContext();
  const [isLogin, setIsLogin] = useState(true);
  const { mutate: signUp } = useSignUpUser();
  const { mutate: signIn } = useSignInUser();
  const switchAuthModeHandler = () => {
    setIsLogin((prevState) => !prevState);
  };
  const navigate = useNavigate();
  useEffect(() => {
    if (isLoggedIn) {
      navigate(PROFILE_ROUTE);
    }
  }, [isLoggedIn]);

  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema: Yup.object({
      email: Yup.string()
        .email(INVALID_EMAIL_ADDRESS)
        .required(NO_EMAIL_PROVIDED),
      password: Yup.string()
        .required(NO_PASSWORD_PROVIDED)
        .min(6, PASSWORD_IS_SHORT),
    }),
    onSubmit: (values) => {
      const user = { ...values, returnSecureToken: true };

      if (!isLogin) {
        signUp(user);
      } else {
        signIn(user);
      }
    },
  });
  return (
    <div>
      <section className={'auth'}>
        <h1>{isLogin ? 'Login' : 'Sign Up'}</h1>
        <form>
          <div className={'control'}>
            <label htmlFor="email">Your Email</label>
            <input
              type="email"
              id="email"
              required
              value={formik.values.email}
              onChange={formik.handleChange}
            />
            {formik.errors.email ? <p>{formik.errors.email}</p> : null}
          </div>
          <div className={'control'}>
            <label htmlFor="password">Your Password</label>
            <input
              type="password"
              id="password"
              required
              value={formik.values.password}
              onChange={formik.handleChange}
            />
            {formik.errors.password ? <p>{formik.errors.password}</p> : null}
          </div>
          <div className={'actions'}>
            <button type="submit" onClick={formik.handleSubmit}>
              {isLogin ? 'Login' : 'Create Account'}
            </button>
            <button
              type="button"
              className={'toggle'}
              onClick={switchAuthModeHandler}
            >
              {isLogin ? CREATE_NEW_ACCOUNT : LOGIN}
            </button>
          </div>
        </form>
      </section>
    </div>
  );
};
